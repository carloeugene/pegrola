const express = require('express')
var multer = require('multer');
var bodyParser = require('body-parser');

const app = express()
app.use(bodyParser.json())

var Storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "./images")
  },
  filename: (req, file, callback) => {
    callback(null, req.params.name + '_' + file.originalname)
  }
})

var upload = multer({
  storage: Storage
}).single('image')

app.post('/r/brustor/brustor/de/designer/image/:name', (req, res) => {
  upload(req, res, err => {
    if(err) {
      console.log(err)
      return res.end("Something went wrong")
    }

    return res.json({
      url: `/images/${req.params.name}_${req.file.originalname}`
    })
  })
})

app.get('/images/:name', (req, res) => res.sendFile(__dirname + `/images/${req.params.name}`))

app.use(express.static('public'))

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
var ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

module.exports = app ;